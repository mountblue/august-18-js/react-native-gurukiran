import React from 'react';
import { Alert,StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native';

export default class App extends React.Component {
  constructor(){
    super();
    this.state={
      count:0,
      stopButton:true,
    }
    this.handleStop=this.handleStop.bind(this);
    this.handleStart=this.handleStart.bind(this);
    this.interval
  }
  componentDidMount(){
    let app=this;
    if(this.state.stopButton)
      this.interval = setInterval(()=>{
      app.setState({
        count:app.state.count+1,
      })
    },1000)

  }
  componentWillUnmount() {
    clearInterval(this.interval)
  }
  handleStop=()=>{
    this.setState({
      stopButton:false
    })
  }

  handleStart=()=>{
    this.setState({
      stopButton:true
    })
  }
  render() {
    if(this.state.stopButton){
     return(
      <View style={styles.container}>
      <Text style={styles.countStyle}>{this.state.count}</Text>
      <Button onPress={this.handleStop} title="STOP"/>
      </View>
     )
    }
    else return (
    <View style={styles.container}>
    {/* <Text style={styles.countStyle}>{this.state.count}</Text> */}
    <Button onPress={this.handleStart} title="START"/>
    </View>)
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0234',
    alignItems: 'center',
    justifyContent: 'center',
  },
  countStyle:{
    fontSize: 50
  }
});

